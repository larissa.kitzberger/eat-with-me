

describe('login', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/')
    })

    it('view', () => {
        cy.get(`[data-testid='header'`).contains('EAT WITH ME');
        cy.get(`[data-testid='signIn'`).contains('Sign in');
    })

    it('errors at login', function () {

        cy.get('input[name=email]').type('admin')
        cy.get('input[name=password]').type('123{enter}')

        cy.get(`[data-testid='home'`).contains('')


    })

    it('successful login', function () {

        cy.get('input[name=email]').type('julia.hainz67@gmail.com')
        cy.get('input[name=password]').type('Passwort123')
        cy.get('form').submit()

        cy.get(`[data-testid='home'`).contains('Welcome to your Eat with Me application ')
        cy.get(`[data-testid='home'`).should('contain', 'julia')

    })

    it('possible to visit /date-detail/0', function () {
        cy.visit('/date-detail/0')
        cy.get('h1').should('contain', 'Dinner Tonight:')
    })

    it('possible to visit /recipe-search', function () {
        cy.visit('/recipe-search')
        cy.get('h2').should('contain', 'Filter')
    })

    })

