

describe('Cooking Date Detail', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/login')
        cy.get('input[name=email]').type('lucy.schach@gmail.com')
        cy.get('input[name=password]').type('Test123!')
        cy.get('form').submit()
        cy.wait(1000)
        cy.visit('http://localhost:3000/date-detail/2606fd00-a859-11eb-9240-550389e8807c')
    })

    it ('edit address', () => {
        cy.get(`[data-testid='editAddress'`).contains('Edit')
        cy.get(`[data-testid='editAddress'`).contains('Softwarepark')
        cy.get(`[data-testid='editAddress'`).contains('2')
        cy.get(`[data-testid='editAddress'`).contains('Edit')
        cy.get(`[data-testid='editUsers'`).contains('Edit')

        cy.get(`[data-testid='openEditAddress'`).click()
        cy.get(`[data-testid='editNumber'`).clear()
        cy.get(`[data-testid='editNumber'`).type('12')
        cy.get(`[data-testid='saveNewAddress'`).click()
        cy.get(`[data-testid='editAddress'`).contains('12')
        cy.get(`[data-testid='openEditAddress'`).click()
        cy.get(`[data-testid='editNumber'`).clear()
        cy.get(`[data-testid='editNumber'`).type('2')
        cy.get(`[data-testid='saveNewAddress'`).click()
        cy.get(`[data-testid='editAddress'`).contains('2')
    })

    it ('edit user', () => {
        cy.visit('http://localhost:3000/date-detail/2606fd00-a859-11eb-9240-550389e8807c')

        cy.get(`[data-testid='editUsers'`).contains('Edit')
        cy.get(`[data-testid='openEditUsers'`).click()
        cy.get(`[data-testid='autocomplete'`).click()
    })
})


