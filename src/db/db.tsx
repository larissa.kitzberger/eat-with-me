import firebase from "firebase";
import {CookingDate, Ingredient, Recipe} from '../types/cooking-date';
import {Fave, User} from '../types/user';
import { Address} from '../types/address';
import {nothing} from "immer";

/**
 * Helper methods for working with the database
 */


export const getCookingDateById = (id: string): Promise<CookingDate> | null => {
    const database = firebase.database().ref();
    return database.child('cooking-dates').child(id).get().then((snapshot) => {
        if (snapshot.exists()) {
            return snapshot.val()
        } else {
            return null
        }
    }).catch(error => {
        console.error(error)
        return null
    })
}


export const getAllCookingDates = (): Promise<CookingDate[]> => {
    const database = firebase.database().ref()
    return database.child('cooking-dates').get().then((snapshot) => {
        if (snapshot.exists()) {
            return snapshot.val()
        } else {
            return null
        }
    }).catch(error => {
        console.error(error)
        return null
    })
}

export const getUserById = (id: string): Promise<User> => {
    const database = firebase.database().ref();
    return database.child('users').child(id).get().then((snapshot) => {
        if (snapshot.exists()) {
            return snapshot.val()
        } else {
            return null
        }
    }).catch(error => {
        console.error(error)
        return null
    })
}

export const getAllUsers = (): Promise<User[]> => {
    const database = firebase.database().ref();
    return database.child('users').get().then((snapshot) => {
        if (snapshot.exists()) {
            return snapshot.val()
        } else {
            return null
        }
    }).catch(error => {
        console.error(error)
        return null
    })
}



export const saveUser = (userId: string, user:{}):Promise<void> => {
    return firebase.database().ref('/users/' + userId ).set(user)
        .then(()=>{
            console.log("created new User");
        })
        .catch(error =>{
            console.error(error)
        })

}

export const setRecipeToCookingDate = (cookingDateId: string, recipe: Recipe): Promise<void> => {
    const database = firebase.database().ref();
    return firebase.database().ref('/cookingdates/' + cookingDateId + 'recipes' + '0' ).set(recipe)
        .then(()=>{
            console.log("set Recipe to Cooking date");
        })
        .catch(error =>{
            console.error(error)
        })
}


export const createCookingDateByUser = (userId: string, cookingDateId:string, cookingDate:CookingDate):Promise<void> => {
    return firebase.database().ref('/users/' + userId +'/cookingdates/' + cookingDateId).set(cookingDate)
        .then(()=>{
            console.log("created new CookingDate");
        })
        .then(()=>{
            firebase.database().ref('/cooking-dates/' + cookingDateId).set(cookingDate)
        })
        .catch(error =>{
            console.error(error)
        })

}



export const updateCookingDateUsers = (cookingDateId: string, newUsers: {id: string}[]): Promise<void> => {
    return firebase.database().ref('cooking-dates/' + cookingDateId + '/users').set(newUsers)
}

export const updateCookingDateAddress = (cookingDateId: string, newAddress: Address): Promise<void> => {
    return firebase.database().ref('cooking-dates/' + cookingDateId + '/address').set(newAddress)
}

export const updateCookingDateIngredient = (cookingDateId: string, ingredientId:string, newUser: string): Promise<void> => {
    return firebase.database().ref('cooking-dates/' + cookingDateId + '/recipes/0/ingredients/' + ingredientId + '/user').set(newUser)
        .then(()=>{
            console.log('changed Ingredient checkbox cooking-dates/' + cookingDateId + '/recipes/0/ingredients/' + ingredientId + '/user');
        })
        .catch(error =>{
            console.error(error)
        })
}

export const updateFavorites = (userId: string, faveId:string, newFaveId: Fave|null): Promise<void> => {
    return firebase.database().ref('users/' + userId + '/favourites/' + faveId ).set(newFaveId)
        .then(()=>{
            console.log('changed Fave users/' + userId + '/favorites/' + faveId);
        })
        .catch(error =>{
            console.error(error)
        })
}





