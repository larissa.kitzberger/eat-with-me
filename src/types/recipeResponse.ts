import {User} from "../types/user";

export type Result = {
    results: RecipeLong[],
    offset:number,
    number:number,
    totalResults:number
}

export type RecipeShort = {
    id: number,
    title: string,
    image: string,
    imageType: string
}

export type RecipeCookingDate = {
    id: number,
    ingredients:[{
        id: number,
        name: string,
        localizedName: string,
        image: string,
    }],
}

export type RecipeLong = {
    vegetarian: boolean,
    vegan: boolean,
    glutenFree: boolean,
    dairyFree: boolean,
    veryHealthy: boolean,
    cheap: boolean,
    veryPopular: boolean,
    sustainable: boolean,
    weightWatcherSmartPoints: number,
    gaps: string,
    lowFodmap: boolean,
    aggregateLikes: number,
    spoonacularScore: number,
    healthScore: number,
    creditsText:string,
    license: string,
    sourceName: string,
    pricePerServing: number,
    extendedIngredients: ExtendedIngredient[]
    id: number,
    title: string,
    readyInMinutes: number,
    servings: number,
    sourceUrl: string,
    image: string,
    imageType: string,
    summary: string,
    cuisines: string[],
    dishTypes: string[],
    diets: string[],
    occasions: string[],
    analyzedInstructions:[{
        name:string,
        steps:ExtendedSteps[]}
    ],
    spoonacularSourceUrl: string,
    usedIngredientCount: number,
    missedIngredientCount: number,
    missedIngredients: ExtendedIngredient[],
    likes: number,
    usedIngredients: []
    unusedIngredients: []
}

export type ExtendedIngredient = {
    id: number,
    airsle: string,
    image: string,
    consistency: string,
    name: string,
    nameClean: string,
    original: string,
    originalString: string,
    amount: number,
    unit: string,
    meta: [],
    metaInformation: []
    measures:[{
        us:{
            amount: number,
            unitShort: string,
            unitLong: string
        },
        metric:{
            amount:number,
            unitShort: string,
            unitLong: string
        }
    }]
}

export type ExtendedSteps = {
    number:number,
    step: string,
    ingredients:[{
        id: number,
        name: string,
        localizedName: string,
        image: string,
    }],
    equipment:[{
        id:number,
        name:string,
        localizedName:string,
        image:string,
    }]}

export type RandomRecipe = {
    recipes:
        [{
            id: number,
            title: string,
            image: string,
            servings: string,
            readyInMinutes: string,
            currentUser:User,
            recipeDescription: string
            ingredients: any
            instructions: []
        }]
}