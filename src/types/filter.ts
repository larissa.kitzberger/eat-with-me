export type Filter = {
    name: string,
    filterSet: boolean
}
export type ToggleComplete = (selectedFilter: Filter) => void;

export type Search ={
    searchTerm: string
}
