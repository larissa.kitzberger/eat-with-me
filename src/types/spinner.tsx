import styled from "styled-components";
import {CircularProgress} from '@material-ui/core';

export const Spinner = styled(CircularProgress)`
    ${({theme}) => `
        display: block;
        margin: 50px auto;
    `}
`;
