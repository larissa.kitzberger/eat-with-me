import {Address} from './address';
import {CookingDate} from "./cooking-date";

export type User = {
    id: number,
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    address: Address,
    favourites: Fave[],
    cookingdates:[
            {
                id: string
            }
        ],
}

export type Fave = {
    id: string,
    recipeName: string
}

export type FavesList = Fave[]

