import {Ingredient} from "./cooking-date";

export type ToggleComplete = (selectedIngredient: Ingredient) => void;
