export type Address = {
    street: string,
    number: number,
    postcode: number,
    city: string
}
