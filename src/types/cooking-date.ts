import {Address} from './address';

export type CookingDate = {
    id: string,
    date: string,
    time: string,
    address: Address,
    users: [
        {
            id: string
        }
    ],
    recipes: Recipe
}

export type Recipe = {
    id: number,
    title: string,
    image: string,
    imageType: string,
    servings: number,
    extendedIngredients: Ingredient[]
    readyInMinutes: number,
}

export type Ingredient = {
    id: number,
    name: string,
    amount: number,
    unit: string,
    user: string
}

export type Instructions = [{
    name: string,
    steps: [
        {
            number: string,
            step: string
        }
    ]
}]
