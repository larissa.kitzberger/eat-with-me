import {User} from '../types/user';
import {createSlice, PayloadAction} from '@reduxjs/toolkit';

type UserState = {
    users: User[],
    showOnlyAvailable: boolean
}

const initialState: UserState = {
    users: [],
    showOnlyAvailable: false
}

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        addUser(state, action: PayloadAction<string>) {
            console.log(state)
            console.log(action)
        }
    }
})

export default userSlice.reducer

export const { addUser } = userSlice.actions
