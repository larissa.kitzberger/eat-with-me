import React from 'react';
import { Route, Redirect } from "react-router-dom";

// @ts-ignore
const GuardedRoute = ({ component: Component, auth, ...rest }) => (
    <Route {...rest} render={(props) => (
        auth !== null
            ? <Component {...props} />
            : <Redirect to='/login' />
    )} />
)

export default GuardedRoute;
