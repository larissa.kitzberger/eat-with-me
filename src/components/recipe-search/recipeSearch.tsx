import React, {Fragment, useEffect,useState} from "react";
import firebase from "firebase";

import {Typography, Grid, Container, TextField} from '@material-ui/core';
import Checkbox from "@material-ui/core/Checkbox";
import SearchIcon from '@material-ui/icons/Search';

import {StyledButton} from '../../shared/styles/button';

import {User} from '../../types/user';
import {Result, ExtendedSteps, RecipeLong} from "../../types/recipeResponse";

import {getRecipesBySearchTerm} from '../recipe-api/recipe-api';
import {getUserById} from "../../db/db";

import {Login} from "../authentication/login";
import {EatWithMeAppBar} from "../../shared/components/appBar"
import {RecipePreview} from "../../shared/components/recipePreview";
import {nothing} from "immer";
import {each} from "immer/dist/utils/common";
import {strict} from "assert";


export const RecipeSearch:React.FC = () => {
    let currentFirebaseUser = firebase.auth().currentUser;
    const [result, setResult] = useState<Result>()
    const [searchTerm, setSearchTerm] = useState('')
    const [vegetarianFilter, setVegetarianFilter] = useState<boolean>(false)
    const [veganFilter, setVeganFilter] = useState<boolean>(false)
    const [dairyFreeFilter, setDairyFreeFilter] = useState<boolean>(false)
    const [dietQuery,setDietQuery] = useState<string>('')
    const [intolerancesQuery, setIntolerancesQuery] = useState<string>('')
    const [currentUser, setCurrentUser] = useState<User>();
    const [openedRecipeInfo, setOpenedRecipeInfo] = useState<boolean>(false)
    const [returnStrings, setReturnStrings] = useState<string[]> ([]);


    useEffect( () => {
        if (currentFirebaseUser) {
            console.log('getFirebaseUser')
            getUserById(currentFirebaseUser?.uid).then(response => {
                setCurrentUser(response);
                console.log('saveCurrentUser')
            }).catch(error => console.error(error))
        }

    },[])


    const handleSearchTerm = (event:any) => {
        setSearchTerm(event.target.value);
    };

    const handleVegetarianFilter = (event:any) => {
        setVegetarianFilter(!vegetarianFilter)
    }
    const handleVeganFilter = (event:any) => {
        setVeganFilter(!veganFilter)
    }
    const handleDairyFreeFilter = (event:any) => {
        setDairyFreeFilter(!dairyFreeFilter)
    }


    const searchRecipes = () => {
        if(vegetarianFilter){
            setDietQuery('&diet=vegetarian')
        }
        if(veganFilter) {
            setDietQuery('&diet=vegan')
        }
        if(dairyFreeFilter){
            setIntolerancesQuery('&intolerances=dairy')
        }

        getRecipesBySearchTerm(searchTerm, dietQuery, intolerancesQuery).then(response => {
            if (response) {
                console.log(response)
                setResult(response)}
        })
    };

    const createInstructionsStringArray = (recipe: RecipeLong): [string] => {
        let returnStringArray: [string] = ["Step by step Instructions"]
        recipe.analyzedInstructions.map(allSteps =>
            allSteps.steps.map(step =>{
                returnStringArray.push(step.step);
            })
        )
        return returnStringArray
    }




    const closeRecipeDetailView = () => {
        setOpenedRecipeInfo(false)
    }

    if(currentUser) {
        return (
            <Fragment>
                <EatWithMeAppBar siteName={"Recipe Search"}/>
                <Container>
                    <br/>
                    <Typography variant="h6" component="h2">Filter</Typography>
                    <br/>
                    <Typography variant="body1">
                        <label>
                            <Checkbox checked={vegetarianFilter} onChange={handleVegetarianFilter}/>{'vegetarian'}
                            <Checkbox checked={veganFilter} onChange={handleVeganFilter}/>{'vegan'}
                            <Checkbox checked={dairyFreeFilter} onChange={handleDairyFreeFilter}/>{'dairy free'}
                        </label>
                    </Typography>
                    <br/>
                    <Typography variant="h6" component="h2">Search</Typography>
                    <br/>
                    <TextField id="searchField" label="e.g. pizza" variant="outlined" onChange={handleSearchTerm}/>
                    <br/>
                    <StyledButton size="large" onClick={() => searchRecipes()}>Search<SearchIcon/>
                    </StyledButton>
                    <br/>
                    <br/>
                    <Grid container spacing={4}>
                        {result?.results.map(currentRecipe =>
                            <Grid item xs={12} sm={4} xl={3}>
                                <RecipePreview id={currentRecipe.id.toString()}
                                               title={currentRecipe?.title}
                                               image={currentRecipe?.image}
                                               servings={currentRecipe?.servings.toString()}
                                               readyInMinutes={currentRecipe?.readyInMinutes.toString()}
                                               currentUser={currentUser}
                                               recipeDescription={currentRecipe?.summary}
                                               ingredients={currentRecipe?.extendedIngredients}
                                               instructions={createInstructionsStringArray(currentRecipe)}/>

                            </Grid>
                        )}
                    </Grid>
                </Container>
                <br/>
                <br/>
                <br/>
            </Fragment>

        )
    } else {
        return (
            <Login/>
        )
    }

}


