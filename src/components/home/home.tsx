import firebase from "firebase"
import React, {Fragment, useEffect, useState} from 'react';
import {Login} from "../authentication/login";
import Button from "@material-ui/core/Button/Button";
import Container from "@material-ui/core/Container/Container";
import {Card, createStyles, Grid, Theme, Typography} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Paper from "@material-ui/core/Paper/Paper";
import {getAllCookingDates, getCookingDateById} from "../../db/db";
import {User} from "../../types/user";
import {CookingDate, Recipe} from "../../types/cooking-date";
import {CookingDates} from "../cooking-dates/cooking-dates";
import {CreateCookingDate} from "../cooking-dates/create-cooking-date";
import {getRandomRecipes, getRecipesBySearchTerm} from "../recipe-api/recipe-api";
import {RandomRecipe, Result} from "../../types/recipeResponse";
import {RecipeCard} from "./recipeCard";



export const HomeScreen = () => {



    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            root: {
                flexGrow: 1,
            },
            paper: {
                padding: theme.spacing(2),
                textAlign: 'center',
                color: theme.palette.text.secondary,
            },
            recipesRandomContainer:{
                backgroundColor: '#b5c99a69',
                padding:'1vw',
            }
        }),
    );

    const classes = useStyles();

    let currentUser = firebase.auth().currentUser;

    const [result, setResult] = useState<RandomRecipe>();


    const handleSignOut = (event:any) => {
        firebase.auth().signOut()
            .then(function () {
                document.location.href = "/login";
            })
            .catch(function (error) {
                console.log(error.message);
            });
    };



    if(currentUser){
        return (
            <Grid>
            <h1 data-testid='home'>Welcome to your Eat with Me application {currentUser.displayName}</h1>

                <Grid item xs={12} className={classes.recipesRandomContainer}>
                    <h2>Your Cooking Dates</h2>
                    <CookingDates/>
                </Grid>

                <Grid item xs={12} className={classes.recipesRandomContainer}>
                    <h2>Some recipes for you</h2>
                    <RecipeCard/>
                </Grid>

                <Grid item xs={12}className={classes.recipesRandomContainer}>
                    <h2>Create a Cooking Date</h2>
                    <CreateCookingDate/>
                </Grid>
            </Grid>

        )
    }
    else{
        return (
            <Login/>
        )

    }
}
