import React, {useEffect, useState} from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import firebase from "firebase";
import {RandomRecipe} from "../../types/recipeResponse";
import {getRandomRecipes} from "../recipe-api/recipe-api";
import {RecipePreview} from "../../shared/components/recipePreview";



export const RecipeCard = () => {

    let currentUser = firebase.auth().currentUser;

    const [result, setResult] = useState<RandomRecipe>();

    const [expanded, setExpanded] = React.useState(false);




    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            root: {
                width: '30vw',
                margin: '1vw'
            },
            media: {
                height: 0,
                paddingTop: '56.25%', // 16:9
            },

            recipeCardsContainer:{
                display: 'flex',
                width: '100%',
                flexWrap: 'wrap',
                margin:'auto',
            },
        }),
    );

    const classes = useStyles();


    useEffect(()=>{

        getRandomRecipes(5).then(response => {
            if(response){
                setResult(response);
            }

        }).catch(error => {
            return console.error(error);
        });
    }, []);

    const handleSignOut = (event:any) => {
        firebase.auth().signOut()
            .then(function () {
                document.location.href = "/login";
            })
            .catch(function (error) {
                console.log(error.message);
            });
    };




    const card = result?.recipes.map((res) =>
        <Card key={res.id} className={classes.root}>
            <CardHeader
                title={res.title}
                subheader={
                    res.readyInMinutes +
                        " Minutes"
                }
            />
            <CardMedia
                className={classes.media}
                image={res.image}
                title={res.title}
            />
        </Card>

    );


        return (
            <div className={classes.recipeCardsContainer}>
                {card}
            </div>
        );


    }
