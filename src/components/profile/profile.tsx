import firebase from "firebase";
import React, {useEffect, useState} from 'react';
import {Login} from "../authentication/login";
import {getCookingDateById, getUserById} from "../../db/db";
import Container from "@material-ui/core/Container/Container";
import {User} from "../../types/user";
import {CardActions, CardContent, Grid, Typography} from "@material-ui/core";
import {StyledDetailCard} from "../cooking-dates-detail/styledDetailCard";

export const Profile = () => {

    let currentUser = firebase.auth().currentUser;
    let [user, setUser] = useState<User>();

    let firstname:string;
    let lastname:string;
    let city:string;
    let number:string;
    let postcode:string;
    let street:string;


    if(currentUser){
        getUserById(currentUser.uid.toString()).then(response => {
            if (response) {
                setUser(response);
            }
        })



        return(
            <Container>
                <h1>
                   Hello {user?.firstName} {user?.lastName}!
                </h1>

                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                        <StyledDetailCard data-testid="Adresse">
                            <CardContent>
                                <h2>
                                    Address:
                                </h2>
                                <Typography>
                                    street: {user?.address.street}<br/>
                                    number: {user?.address.number}<br/>
                                    postcode: {user?.address.postcode}<br/>
                                    city: {user?.address.city}<br/>
                                </Typography>
                            </CardContent>
                        </StyledDetailCard>
                    </Grid>
                </Grid>

            </Container>
        )
    }

    else{
        return <Login/>
    }


};
