/**
 * Helper component with methods to retrieve information from the API
 */
import {Instructions, Recipe} from '../../types/cooking-date';
import {Result, RecipeShort, RandomRecipe} from '../../types/recipeResponse';

const apiKey: string = '66a4557fd8e649518b1773c9850155d2';
const apiKey2: string = 'a75a4d6dbdc8475685e8c18ee5b17811';
const apiURL: string = 'https://api.spoonacular.com';

export const getRecipeById = (recipeId: string): Promise<Recipe | null> => {
    return fetch(apiURL + '/recipes/' + recipeId + '/information?includeNutrition=false&apiKey=' + apiKey)
        .then(response => {
            return response.json().then(data => data)
        }).catch(error => {
            console.error(error)
            return null
        })
}

export const getRecipeInstructionsById = (recipeId: string): Promise<Instructions | null> => {
    return fetch(apiURL + '/recipes/' + recipeId + '/analyzedInstructions?stepBreakdown=false&apiKey=' + apiKey)
        .then(response => {
            return response.json().then(data => data)
        }).catch(error => {
            console.error(error)
            return null
        })
}

export const getRecipesBySearchTerm = (searchTerm: string, diet: string, intolerances: string): Promise<Result> => {
    return fetch(apiURL + '/recipes/complexSearch?query=' + searchTerm + diet + intolerances + '&number=7&fillIngredients=true&addRecipeInformation=true&apiKey=' + apiKey)
        .then(response => {
            return response.json().then(data => data)
            console.log(apiKey)
        }).catch(error => {
            console.error(error);
            return null;
        })
};

export const getRandomRecipes = (numbers: number): Promise<RandomRecipe> => {
    return fetch(apiURL + '/recipes/random?number='+ numbers + '&apiKey=' + apiKey2)
        .then(response => {
            return response.json().then(data => data)
        }).catch(error => {
            console.error(error);
            return null;
        })
};
