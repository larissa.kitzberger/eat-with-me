import React, {useEffect, useState} from "react";
import { Ingredient,Recipe,CookingDate } from '../../types/cooking-date';
import {User} from '../../types/user';
import {Typography, Checkbox, FormGroup, FormControlLabel, Chip, Theme, createStyles} from '@material-ui/core';
import {ToggleComplete} from "../../types/ingredient";
import {getCookingDateById, getUserById, updateCookingDateIngredient} from "../../db/db";
import {useParams} from "react-router-dom";
import {Params} from "./cooking-dates-detail";
import ScheduleIcon from "@material-ui/icons/Schedule";
import firebase from "firebase";
import {makeStyles, withStyles} from "@material-ui/core/styles";
import {CheckboxProps} from "@material-ui/core/Checkbox";
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import ShoppingBasketOutlinedIcon from '@material-ui/icons/ShoppingBasketOutlined';
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import Favorite from "@material-ui/icons/Favorite";
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';

export interface ingredientListItemProps {
    ingredient: Ingredient;
    recipe: Recipe;
    cookingDateId: string;
    ingredientIndex: string;
}


const ShoppingCheckbox = withStyles({
    root: {
        color: '#283618',
        '&$checked': {
            color: '#283618',
        },
    },
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

export const IngredientListItem:React.FC<ingredientListItemProps> = ({ ingredient,ingredientIndex,cookingDateId}) => {
    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            nameStyle: {
                color: '#001524',
                lineHeight: '1.0',
                textAlign: 'center',
            },
            ingredientsStyle: {
                color: '#f4f4f9',
                lineHeight: '1',
                textAlign: 'center',
            }
        }),
    );

    const classes = useStyles();

    let currentFirebaseUser = firebase.auth().currentUser;
    const [cookingDate, setCookingDate] = useState<CookingDate>()
    const [user, setUser] = useState<string>(ingredient.user)
    const [currentUserName, setCurrentUserName] = useState<string>('unknown');
    const [chipColor, setChipColor] = useState<string>('primary')




    if(currentFirebaseUser){
        getUserById(currentFirebaseUser?.uid).then(response => {
            if (response) {
                setCurrentUserName(response.firstName);
            }
        }).catch(error => console.error(error))
    }

    const toggleIngredient = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (user == '') {
            setUser(currentUserName);
            setChipColor("secondary")
            updateCookingDateIngredient(cookingDateId.toString(), ingredientIndex, currentUserName).then(() => {
            }).catch(error => console.error(error))
        } else {
            updateCookingDateIngredient(cookingDateId.toString(), ingredientIndex, '').then(() => {
               setUser('');
            }).catch(error => console.error(error))
        }
        return user;
    };

    return (
            <Typography>
                    <FormControlLabel
                        control={<ShoppingCheckbox checked={user !== ''}
                                                   icon={<ShoppingBasketOutlinedIcon/>} checkedIcon={<ShoppingBasketIcon/>}
                                                   onChange={toggleIngredient}
                                                   name="ingredientCheck"/>
                                                   }
                        label={<Chip label={
                            <section>
                                <div className={classes.ingredientsStyle}>{ingredient.amount + ingredient.unit + ' ' +
                                    ingredient.name} </div>
                                <div className={classes.nameStyle}>{user}</div>

                            </section>
                        } color={"secondary"}/>}
                    />
            </Typography>
    )
}
