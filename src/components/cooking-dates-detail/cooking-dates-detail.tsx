import {useParams} from 'react-router-dom';
import {useState} from 'react';
import React, {Fragment, useEffect} from "react";

import produce from 'immer';

import {Typography, CardContent, Grid, CardActions, List, Container, Chip, ListItem, Theme, createStyles} from '@material-ui/core';
import makeStyles from "@material-ui/core/styles/makeStyles"
import PlaceOutlinedIcon from '@material-ui/icons/PlaceOutlined';
import PeopleIcon from '@material-ui/icons/People';
import ScheduleIcon from '@material-ui/icons/Schedule';
import GroupIcon from '@material-ui/icons/Group';

import {CookingDate, Instructions, Recipe} from '../../types/cooking-date';
import {Spinner} from '../../types/spinner';
import {User} from '../../types/user';
import {Address} from '../../types/address';

import {getCookingDateById, getUserById, updateCookingDateAddress, updateCookingDateUsers} from '../../db/db';
import {getRecipeById, getRecipeInstructionsById} from '../recipe-api/recipe-api';

import {SnackBar} from '../../shared/components/snackBar';
import {EditUsers} from './edit-users';

import {EditAddress} from './edit-address';
import {IngredientListItem} from  './ingredientListItem';

import {StyledButton} from '../../shared/styles/button';
import {StyledDetailImage} from './styledDetailImage';
import {StyledDetailCard} from './styledDetailCard';
import {EatWithMeAppBar} from "../../shared/components/appBar";

export type Params = {
    id: string
}

export const CookingDatesDetail = () => {
    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            ingredientsBlock: {
                width: 'fitContent',
            },
            divIngredients: {
                display: 'flex',
                flexWrap: 'wrap',
            }
        }),
    );

    const classes = useStyles();

    const params: Params = useParams()
    const [cookingDate, setCookingDate] = useState<CookingDate>()
    const [instructions, setInstructions] = useState<Instructions>()
    const [recipe, setRecipe] = useState<Recipe>()
    let [users, setUsers] = useState<User[]>([])

    const [openedEditAddress, setOpenedEditAddress] = useState<boolean>(false)
    const [openedEditUsers, setOpenedEditUsers] = useState<boolean>(false)

    const [openedSuccessSnackBar, setOpenedSuccessSnackBar] = useState<boolean>(false)
    const [openedErrorSnackBar, setOpenedErrorSnackBar] = useState<boolean>(false)

    const [openedSuccessSnackBar2, setOpenedSuccessSnackBar2] = useState<boolean>(false)
    const [openedErrorSnackBar2, setOpenedErrorSnackBar2] = useState<boolean>(false)

    useEffect(() => {
        getCookingDateById(params.id)?.then((response) => {
            setCookingDate(response)
            const recipeId: string = response.recipes.id.toString();

            response.users.forEach(userId => {

                getUserById(userId.toString()).then(user => {
                    setUsers(produce(draftState => {
                        if (!users.find(element => element.id === user.id)) {
                            draftState.push(user)
                        }
                    }))
                }).catch(error => console.error(error))
            })

            if (recipeId) {
                getRecipeById(recipeId.toString()).then(response => {
                    if (response) {
                        setRecipe(response)
                    }
                }).catch(error => console.error(error))

                getRecipeInstructionsById(recipeId.toString()).then(response => {
                    console.log(response)
                    if (response) {
                        setInstructions(response)

                    }
                }).catch(error => console.error(error))
            }

        }).catch(error => console.error(error))
    }, [])


    const closeEditAddress = (newAddress: Address, save: boolean) => {
        setOpenedEditAddress(false)
        if (save && cookingDate) {
            setCookingDate(
                produce(draftState => {
                        if (draftState) {
                            draftState.address = newAddress
                        }
                    }
                ))
            // cookingDate.address = newAddress
            updateCookingDateAddress(cookingDate.id.toString(), newAddress).then(() => {
                setOpenedSuccessSnackBar(true)
            }).catch(error => {
                setOpenedErrorSnackBar(true)
                console.error(error)
            })
        }
    }

    const closeEditUsers = (newUser: User, save: boolean = false, deleteUserId: number, deleteUser: boolean) => {
        setOpenedEditUsers(false)
        if (save) {
            setUsers(produce(draft => {
                draft.push(newUser)
            }))
            if (cookingDate) {
                const userIds: [{ id: string}] = cookingDate.users
                userIds.push({id: newUser.id.toString()})
                updateCookingDateUsers(cookingDate.id.toString(), userIds).then(() => {
                    setOpenedSuccessSnackBar2(true)
                }).catch(error => {
                    setOpenedErrorSnackBar2(true)
                    console.error(error)
                })
            }
        }
        if (deleteUser && cookingDate) {
            const userIds: { id: string }[] = cookingDate.users
            let newUserIds: { id: string }[] = []
            userIds.forEach(element => {
                if (element.id !== deleteUserId.toString()) {
                    newUserIds.push(element)
                }
            })

            updateCookingDateUsers(cookingDate.id.toString(), newUserIds).then(() => {
                setOpenedSuccessSnackBar2(true)
            }).catch(error => {
                setOpenedErrorSnackBar2(true)
                console.error(error)
            })

            let newUsers: User[] = []
            users.forEach(element => {
                if (element.id !== deleteUserId) {
                    newUsers.push(element)
                }
            })
            users = newUsers
            setUsers(newUsers)
        }
    }


    return (
        <Fragment>
            <EatWithMeAppBar siteName={'Your Cooking Date'} />
            {(!cookingDate || !recipe || !instructions) && (
                <Spinner/>
            )}
            {cookingDate && recipe && instructions && (
                <Container>
                    <Grid container
                          direction="row"
                          justify="space-evenly"
                          alignItems="stretch"
                          spacing={2}>

                        <Grid item xs={12} sm={12}>
                            <Typography variant="h4" component="h1">{recipe?.title}</Typography>
                            <Typography variant="h6" component="h1">{cookingDate?.date} at {cookingDate?.time}</Typography>
                            <Chip icon={<ScheduleIcon/>} label={recipe?.readyInMinutes + ' min'} color="primary"/>&nbsp;
                            <Chip icon={<GroupIcon/>} label={recipe?.servings} color="primary"/>
                            <br/>
                            <br/>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <StyledDetailCard data-testid="editUsers">
                                <CardContent>
                                    <PeopleIcon/>
                                    <Typography>
                                        {users.map(p => <span
                                            key={p.id}>{p.firstName} {p.lastName}<br/></span>)}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <StyledButton size="small"  data-testid="openEditUsers"
                                                  onClick={() => setOpenedEditUsers(true)}>Edit</StyledButton>
                                    <EditUsers users={users} open={openedEditUsers}
                                               onClose={closeEditUsers}/>
                                    <SnackBar message="The participants have been updated successfully!" type="success"
                                              open={openedSuccessSnackBar2} setOpen={setOpenedSuccessSnackBar2}/>
                                    <SnackBar message="The participants could not be updated. Try again later!"
                                              type="error" open={openedErrorSnackBar2} setOpen={setOpenedErrorSnackBar2}/>
                                </CardActions>
                            </StyledDetailCard>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <StyledDetailCard data-testid="editAddress">
                                <CardContent>
                                    <PlaceOutlinedIcon/>
                                    <Typography>
                                        {cookingDate?.address.street} {cookingDate?.address.number}<br/>
                                        {cookingDate?.address.postcode} {cookingDate?.address.city}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <StyledButton size="small" data-testid="openEditAddress"
                                                  onClick={() => setOpenedEditAddress(true)}>Edit</StyledButton>
                                    <EditAddress address={cookingDate?.address} open={openedEditAddress}
                                                 onClose={closeEditAddress}/>
                                    <SnackBar message="Your address has been updated successfully!" type="success"
                                              open={openedSuccessSnackBar} setOpen={setOpenedSuccessSnackBar}/>
                                    <SnackBar message="Your address could not be updated. Try again later!" type="error"
                                              open={openedErrorSnackBar} setOpen={setOpenedErrorSnackBar}/>
                                </CardActions>
                            </StyledDetailCard>
                        </Grid>

                        <Grid item xs={12} sm={12}>
                            <Typography variant="h6" component="h2">Ingredients</Typography>
                                <div className={classes.divIngredients}>
                                    {cookingDate?.recipes.extendedIngredients.map(ingredient =>
                                        <div className={classes.ingredientsBlock} key={ingredient.id} >
                                            <IngredientListItem key={ingredient.id} ingredient={ingredient} recipe={recipe} cookingDateId={cookingDate.id.toString()}
                                                                ingredientIndex={cookingDate?.recipes.extendedIngredients.indexOf(ingredient).toString()} />
                                        </div>
                                    )}
                                </div>

                        </Grid>

                        <Grid container
                              direction="row"
                              justify="space-evenly"
                              alignItems="center">

                            <Grid item xs={10} sm={4}>
                                <StyledDetailImage src={recipe?.image} alt="recipe-image"/>
                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <Typography variant="h6" component="h1">Instructions</Typography>
                                <List component="nav">
                                    {instructions?.map(instruction =>
                                        <div key={instructions?.indexOf(instruction)}>
                                            <Typography variant="body1">{instruction.name}</Typography>
                                            {instruction.steps.map(step =>
                                                <ListItem key={step.number}>
                                                    <Typography variant="body2">{step.number}: {step.step}</Typography>
                                                </ListItem>
                                            )}
                                        </div>
                                    )}
                                </List>
                            </Grid>
                        </Grid>

                    </Grid>
                    <br/><br/>


                </Container>
            )}
        </Fragment>
    )
}
