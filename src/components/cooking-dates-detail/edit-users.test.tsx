import {User} from '../../types/user';
import {theme} from '../../shared/styles/theme';
import {ThemeProvider} from 'styled-components';
import {MuiThemeProvider} from '@material-ui/core';
import React from 'react';
import {EditUsers} from './edit-users';
import {render} from '@testing-library/react'
import firebase from 'firebase';


describe("Edit Users", () => {

    beforeEach(() => {
        /**
         * firebase initialization
         */
        firebase.initializeApp({
            apiKey: "AIzaSyCBGl3zvEF76GnARP8UkpCOfX7B1kak8Ik",
            authDomain: "eat-with-me-1d32e.firebaseapp.com",
            databaseURL: "https://eat-with-me-1d32e-default-rtdb.europe-west1.firebasedatabase.app/",
            projectId: "eat-with-me-1d32e",
            storageBucket: "eat-with-me-1d32e.appspot.com",
            messagingSenderId: "333083075508",
            appId: "1:333083075508:web:4df700df063ccd096c48e1"
        });
    })

    test("template", () => {
        const testUsers: User[] = [
            {
                cookingdates: [{id: ''}],
                id: 0,
                firstName: 'John',
                lastName: 'Doe',
                email: 'test@example.com',
                password: 'secret',
                address: {
                    city: 'Hagenberg', number: 24, postcode: 4232, street: 'Softwarepark'
                },
                favourites: [{id: '0', recipeName: 'name'}]
            }
        ];

        const {getByRole, queryByText, queryByLabelText} = render(
            <MuiThemeProvider theme={theme}>
                <ThemeProvider theme={theme}>
                    <EditUsers users={testUsers} onClose={() => {}} open={true}/>
                </ThemeProvider>
            </MuiThemeProvider>
        )

        expect(getByRole).toBeDefined()
        expect(getByRole('dialog')).toBeDefined()
        expect(getByRole('heading')).toBeDefined()
        expect(getByRole('list')).toBeDefined()
        expect(getByRole('listitem')).toBeDefined()
        expect(queryByLabelText('delete')).toBeDefined()
        expect(queryByLabelText('Open')).toBeDefined()
        expect(queryByText('John')).toBeDefined()
        expect(queryByText('Doe')).toBeDefined()

    })
})
