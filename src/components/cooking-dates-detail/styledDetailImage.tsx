import styled from 'styled-components';


export const StyledDetailImage = styled.img`
  
  max-height: auto;
  width: 100%;
  box-shadow: 0px 10px 24px -4px rgba(0,0,0,0.38);
  
  @media only screen and (max-width: 600px) {
    width: 100%;
    clear: both;
    margin-bottom: 30px;
  }
`;
