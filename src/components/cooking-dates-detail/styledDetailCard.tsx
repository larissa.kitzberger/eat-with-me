import styled from 'styled-components';
import {Card} from '@material-ui/core';


export const StyledDetailCard = styled(Card)`
    max-width: 100%;
    display: block;
    margin-top: 10px;
    text-align: center;
`;
