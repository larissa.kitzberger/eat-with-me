import React, {Fragment, useState} from "react";
import produce from 'immer';
import {DialogContent, IconButton, TextField} from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import PersonRoundedIcon from '@material-ui/icons/PersonRounded';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import {User} from '../../types/user';
import {getAllUsers} from '../../db/db';

export interface EditUserProps {
    open: boolean
    users: User[]
    onClose: (value: User, save: boolean, deleteUserId: number, deleteUser: boolean) => void
}

export const EditUsers = ({ open, users, onClose }: EditUserProps) => {
    const [availableUsers, setAvailableUsers] = useState<User[]>([])
    const emptyUser: User = {
        cookingdates: [{id: ''}],
        address: {street: '', number: 0, city: '', postcode: 0},
        email: '', favourites: [{id: '', recipeName: ''}], firstName: '', id: 0,
        lastName: '', password: ''
    }


    const onEnter = () => {
        getAllUsers().then(allUsers => {
            for (const [key, value] of Object.entries(allUsers)) {
                if (!users.find(element => element.id.toString() === key)) {
                    setAvailableUsers(
                        produce(draft => {
                            draft.push(value)
                        })
                    )
                }
            }
        })
    }

    const handleClose = () => {
        onClose(emptyUser, false, 0, false)
        setAvailableUsers([])
    }

    const onUserSelect = (newUser: User | null) => {
        if (newUser) {
            onClose(newUser, true, 0, false)
            setAvailableUsers([])
        }
    }

    const removeUser = (userId: number) => {
        if (userId) {
            onClose(emptyUser, false, userId, true)
            setAvailableUsers([])
        }
    }

    return(
        <Dialog open={open} onClose={() => handleClose()} aria-labelledby="edit-users-overlay"
                maxWidth="xs" fullWidth={true} onEnter={onEnter}>
            <DialogTitle>Edit participants</DialogTitle>
            <DialogContent>
                {users && (
                    <Fragment>
                        <List>
                            {users.map(u =>
                                <ListItem key={u.id}>
                                    <ListItemIcon>
                                        <PersonRoundedIcon />
                                    </ListItemIcon>
                                    <ListItemText primary={`${u.firstName} ${u.lastName}`}/>
                                    <ListItemSecondaryAction slot="end">
                                        <IconButton edge="end" aria-label="delete" onClick={() => removeUser(u.id)}>
                                            <RemoveCircleOutlineIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            )}
                        </List>

                        <Autocomplete renderInput={(params) =>
                            <TextField {...params} variant="outlined" label="Add participant" data-testid="autocomplete"/>
                        } options={availableUsers} getOptionLabel={(option) => {
                            return option.firstName + ' ' + option.lastName
                        }} onChange={(event, newValue: User | null) => onUserSelect(newValue)}/>
                    </Fragment>
                )}
                {!users && (
                    <p>No participants added yet. </p>
                )}
                <br/><br/>
            </DialogContent>
        </Dialog>
    )
}
