import { useForm } from "react-hook-form";
import React from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import {DialogActions, DialogContent, TextField} from '@material-ui/core';

import {Address} from '../../types/address';
import {StyledButton} from '../../shared/styles/button';

export interface EditAddressProps {
    open: boolean;
    address: Address | undefined;
    onClose: (value: Address, save: boolean) => void;
}

export const EditAddress = (props: EditAddressProps) => {
    const { onClose, address, open } = props
    const { register, handleSubmit } = useForm()

    const onSubmit = (data: Address) => {
        onClose(data, true)
    }

    const handleClose = () => {
        if (address) {
            onClose(address, false)
        }
    }


    return(
        <Dialog open={open} onClose={() => handleClose()} aria-labelledby="edit-address-overlay">
            <DialogTitle>Edit address</DialogTitle>
            <DialogContent>
                {address && (
                    <form noValidate autoComplete='off'>
                        <TextField inputRef={register} name="street" defaultValue={address.street} placeholder="street"/>
                        <TextField inputRef={register} name="number" type="number" defaultValue={address.number}
                                   data-testid="editNumber" placeholder="number"/><br/>
                        <TextField inputRef={register} name="postcode" type="number" defaultValue={address.postcode}
                                   placeholder="postcode"/>
                        <TextField inputRef={register} name="city" defaultValue={address.city} placeholder="city"/>
                    </form>
                )}
            </DialogContent>
            <DialogActions>
                <StyledButton onClick={handleSubmit(onSubmit)} data-testid="saveNewAddress">Save</StyledButton>
            </DialogActions>
        </Dialog>
    );
}
