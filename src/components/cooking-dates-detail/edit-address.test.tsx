import { render } from '@testing-library/react'
import React from 'react'
import {EditAddress} from './edit-address';
import {Address} from '../../types/address';
import {MuiThemeProvider} from '@material-ui/core';
import {theme} from '../../shared/styles/theme';
import {ThemeProvider} from 'styled-components';

describe("Edit Address", () => {

    test("template", () => {
        const testAddress: Address = {
            city: 'Hagenberg', number: 24, postcode: 4232, street: 'Softwarepark'

        }

        const { getByRole, queryByText } = render(
            <MuiThemeProvider theme={theme}>
                <ThemeProvider theme={theme}>
                    <EditAddress open={true} address={testAddress} onClose={() => {}}/>
                </ThemeProvider>
            </MuiThemeProvider>
        )
        expect(getByRole).toBeDefined()
        expect(getByRole('dialog')).toBeDefined()
        expect(getByRole('heading')).toBeDefined()

        expect(queryByText('Softwarepark')).toBeDefined()
        expect(queryByText('24')).toBeDefined()
        expect(queryByText('4232')).toBeDefined()
        expect(queryByText('Hagenberg')).toBeDefined()

        const button = getByRole('button')
        expect(button).toBeDefined()
        expect(button.textContent).toEqual('Save')
    })

})
