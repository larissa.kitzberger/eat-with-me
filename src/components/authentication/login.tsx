import firebase from "firebase";
import React, {useState} from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import {Register} from "./register";
import {HomeScreen} from "../home/home";



export const Login = () => {
    function Copyright() {
        return (
            <Typography variant="body2" color="textSecondary" align="center">
                {'Copyright © '}
                <Link color="inherit" href="https://material-ui.com/">
                    Your Website
                </Link>{' '}
                {new Date().getFullYear()}
                {'.'}
            </Typography>
        );
    }

    const useStyles = makeStyles((theme) => ({
        root: {
        },
        image: {
            backgroundImage: 'url("https://source.unsplash.com/collection/988658")',
            backgroundRepeat: 'no-repeat',
            backgroundColor:
                theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
        },
        paper: {
            margin: theme.spacing(8, 4),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        },
        avatar: {
            margin: theme.spacing(1),
            backgroundColor: theme.palette.secondary.main,
        },
        form: {
            width: '100%', // Fix IE 11 issue.
            marginTop: theme.spacing(1),
        },

        submit: {
            margin: theme.spacing(3, 0, 2),
        },
        loginScreenTitle: {
            color:'#222',
            backgroundColor:'#f77f00',
            lineHeight:'90%',
            fontSize: '8rem',
            padding: '1rem',
            textAlign:'center',
            boxShadow:'box-shadow: 3px 3px 55px -16px rgba(0,0,0,0.81);\n' +
                '-webkit-box-shadow: 3px 3px 55px -16px rgba(0,0,0,0.81);\n' +
                '-moz-box-shadow: 3px 3px 55px -16px rgba(0,0,0,0.81);',
            height:'fit-content',
            width:'50px',
            borderRadius: '20px',
            margin:'25% 15%'
        },
    }));

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleEmail = (event:any) => {
        setEmail(event.target.value);
    };

    const handlePassword = (event:any) => {
        setPassword(event.target.value);
    };

    const classes = useStyles();
    const handleSignIn = (event:any) => {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(response => {
                if(response) {
                    document.location.href = "/home";
                }
            }).catch((error) => {
            console.log(error.message);
        });

        event.preventDefault();

    };

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image}>
                <Typography data-testid='header' className={classes.loginScreenTitle} component="h2" variant="h5">
                    EAT WITH ME
                </Typography>
            </Grid>
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>

                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form className={classes.form} onSubmit={handleSignIn} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            onChange={handleEmail}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange = {handlePassword}
                        />
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary" />}
                            label="Remember me"
                        />

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onSubmit={handleSignIn}
                        >
                            Sign In
                        </Button>

                        <Grid container>
                            <Grid item xs>
                                <Link href="#" variant="body2">

                                </Link>
                            </Grid>
                            <Grid item>
                                <Link href="/register" variant="body2">
                                    {"Don't have an account? Sign Up"}
                                    <BrowserRouter>
                                        <Route exact path={"/register"} component={Register} />
                                    </BrowserRouter>
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Grid>
        </Grid>
    )
};
