import firebase from "firebase";
import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';


import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {saveUser} from "../../db/db";




export const Register = (props:any) => {

    const useStyles = makeStyles((theme) => ({
        paper: {
            marginTop: theme.spacing(8),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        },
        avatar: {
            margin: theme.spacing(1),
            backgroundColor: theme.palette.secondary.main,
        },
        form: {
            width: '100%', // Fix IE 11 issue.
            marginTop: theme.spacing(3),
        },
        submit: {
            margin: theme.spacing(3, 0, 2),
        },
        container: {
            backgroundImage: 'url("https://source.unsplash.com/collection/988658")',
            backgroundSize: 'cover',
            margin:'-65px 0',
        },
        backgroundLight: {
            backgroundColor: '#fff',
            margin: 'auto',
            padding: '3.5%'


        },
    }));

    const classes = useStyles();

    //props firstname, lastname, email and password
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [city, setCity] = useState('');
    const [number, setHouseNumber] = useState('');
    const [postcode, setPostcode] = useState('');
    const [street, setStreet] = useState('');




    const handleFirstName = (event:any) => {
        setFirstname(event.target.value);
    };

    const handleLastName = (event:any) => {
        setLastname(event.target.value);
    };

    const handleEmail = (event:any) => {
        setEmail(event.target.value);
    };

    const handlePassword = (event:any) => {
        setPassword(event.target.value);
    };

    const handleCity = (event:any) => {
        setCity(event.target.value);
    };

    const handleHouseNumber = (event:any) => {
        setHouseNumber(event.target.value);
    };

    const handlePostcode = (event:any) => {
        setPostcode(event.target.value);
    };
    const handleStreet = (event:any) => {
        setStreet(event.target.value);
    };


    const handleSignUp = (event:any) => {
        let user:any;
        firebase.auth()
            .createUserWithEmailAndPassword(email, password)
            .then(function(){
                user = firebase.auth().currentUser;
                console.log(user);
                //user.sendEmailVerification();
            })
            .then(function () {
                user.updateProfile({
                    displayName: firstname + " " + lastname
                })

                let newUser = {
                    address: {
                        city: city,
                        number: number,
                        postcode: 4232,
                        street: street
                    },
                    id: user.uid,
                    firstName: firstname,
                    lastName: lastname,
                    favourites: [
                        {
                            id: ''
                        }
                    ],
                    recipes: [],
                }

                saveUser(user.uid.toString(), newUser);
            })
            .then(function () {
               document.location.href = "/login";
            }).catch((error:any) => {
            switch (error.code) {
                case 'auth/email-already-in-use':
                    return(
                        alert("email already in use")
                    );
                case 'auth/invalid-email':
                    return(
                        alert("invalid email")
                    );
                case 'auth/weak-password':
                    return(
                        alert("weak password")
                    );
            }
        });
        event.preventDefault();
    };

    return (
        <Container className={classes.container} component="main">
            <Container  className={classes.backgroundLight}  maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign up
                    </Typography>
                    <form className={classes.form} noValidate  onSubmit = {handleSignUp}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="fname"
                                    name="firstName"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="First Name"
                                    onChange={handleFirstName}
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="lastName"
                                    label="Last Name"
                                    name="lastName"
                                    onChange={handleLastName}
                                    autoComplete="lname"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <h3>Address</h3>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="city"
                                    label="City"
                                    name="city"
                                    onChange={handleCity}
                                    autoComplete="city"
                                />
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="houseNumber"
                                    label="Number"
                                    name="houseNumber"
                                    onChange={handleHouseNumber}
                                    autoComplete="hnumber"
                                />
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="postcode"
                                    label="Postcode"
                                    name="postcode"
                                    onChange={handlePostcode}
                                    autoComplete="pCode"
                                />
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="street"
                                    label="Street"
                                    name="street"
                                    onChange={handleStreet}
                                    autoComplete="street"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    onChange={handleEmail}
                                    autoComplete="email"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    onChange = {handlePassword}
                                    autoComplete="current-password"
                                />
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onSubmit = {handleSignUp}
                        >
                            Sign Up
                        </Button>
                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link href="/login" variant="body2">
                                    Already have an account? Sign in
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        </Container>
    );
};
