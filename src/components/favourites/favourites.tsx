import React, {Fragment, useEffect, useState} from "react";
import {User} from '../../types/user';
import {Typography, Checkbox, Theme, createStyles, Container, Grid, ListItem} from '@material-ui/core';
import {getUserById, updateFavorites} from "../../db/db";
import firebase from "firebase";
import {makeStyles, withStyles} from "@material-ui/core/styles";
import {CheckboxProps} from "@material-ui/core/Checkbox";
import {Login} from "../authentication/login";
import {EatWithMeAppBar} from "../../shared/components/appBar";
import {RecipePreview} from "../../shared/components/recipePreview";
import {getRecipeById, getRecipeInstructionsById} from "../recipe-api/recipe-api";
import {Instructions, Recipe} from "../../types/cooking-date";
import {RecipeLong} from "../../types/recipeResponse";
import produce from "immer";


const FavoriteCheckbox = withStyles({
    root: {
        color: '#b7094c',
        '&$checked': {
            color: '#b7094c',
        },
    },
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);


export const Favourites = () => {
    let currentFirebaseUser = firebase.auth().currentUser;
    const [currentUser, setCurrentUser] = useState<User>();
    const [favourites, setFavourites] = useState<string[]> ([]);
    const [favouriteRecipes, setFavouriteRecipes] = useState<Recipe[]> ([]);
    const [recipesSaved,setRecipesSaved] = useState<boolean>(false)
    const [instructions,setInstructions] = useState<string[][]>([])


    const updateCurrentUser = (currentUserId:string) => {
        getUserById(currentUserId).then(response => {
            setCurrentUser(response);
            if (!favouriteRecipes[0]){
                getFavourites(response)
            }
        }).catch(error => console.error(error))
    }

    useEffect( () => {
        if (currentFirebaseUser) {
            console.log('getFirebaseUser')
            updateCurrentUser(currentFirebaseUser.uid)
        }

        // if (!favouriteRecipes[0]){
        //
        //     getFavourites()
        // }



   },[])



    const getFavourites = (user: User) =>{
        console.log('get favourites method')
        if(user) {
            for (const [key, value] of Object.entries(user.favourites)) {

                console.log('favourite: ' + value.id)

                getRecipeById(value.id).then(response => {
                    if (response) {
                        setFavouriteRecipes(produce(draft => {
                            draft.push(response)
                        }))

                        getRecipeInstructionsById(value.id).then(response => {
                            console.log(response)
                            if (response) {
                                let instructionsTmp: string[] = [""];
                                response.map(steps =>
                                    steps.steps.map(singleStep => {
                                        instructionsTmp.push(singleStep.step)
                                    }))
                                setInstructions(produce(draft => {
                                    draft.push(instructionsTmp)
                                }))
                            }
                        })
                    }
                })
            }
        }
    }

    const fillFaveRecipeArray = () => {
        // getFavourites()
        console.log('in fillFaveRecipeArray')
        if(!recipesSaved){

            console.log('not saved yet ')
            favourites.map( fave =>

                getRecipeById(fave).then(response => {
                    if (response) {
                        console.log('favouriteRecipe: ' + response)
                        console.log(response)
                        favouriteRecipes.push(response)
                    }
                }))

        }


    }



    const createInstructionsStringArray = (id:string): [string] => {
        let returnStringArray: [string] = [""]
        getRecipeInstructionsById(id).then(response => {
            console.log(response)
            if (response) {
                response.map(steps =>
                    steps.steps.map(singleStep =>
                        returnStringArray.push(singleStep.step)
                    ))
            }
        })
        return returnStringArray
    }




    if(currentUser) {
        return (
            <Fragment>
                <EatWithMeAppBar siteName={'Favourite'} />
                <Container>
                    <Grid container direction="row" justify="space-evenly" alignItems="flex-start" spacing={4}>
                        {favouriteRecipes.map( (fave, index) =>

                            <Grid item xs={12} sm={4} xl={3}>
                                <RecipePreview id={fave.id.toString()}
                                               title={fave.title}
                                               image={fave.image}
                                               servings={fave.servings.toString()}
                                               readyInMinutes={fave.readyInMinutes.toString()}
                                               currentUser={currentUser}
                                               recipeDescription={""}
                                               ingredients={null}
                                               //  instructions={instructions[index]}
                                               instructions={['']}
                                />
                            </Grid>
                        )}
                    </Grid>
                </Container>
            </Fragment>
        )

    } else {
        return (<Login/>)
    }
}
