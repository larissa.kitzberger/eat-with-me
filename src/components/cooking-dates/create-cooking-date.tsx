import {Fragment, useEffect, useState} from "react";
import {createCookingDateByUser, getAllCookingDates, getAllUsers, getUserById} from "../../db/db";
import firebase from "firebase";
import {useForm} from "react-hook-form";
import Grid from "@material-ui/core/Grid/Grid";
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button/Button";
import Link from "@material-ui/core/Link/Link";
import React from "react";
import {DialogContent, IconButton, Input, makeStyles, MenuItem, Select, Theme} from "@material-ui/core";
import Container from "@material-ui/core/Container/Container";
import CssBaseline from "@material-ui/core/CssBaseline/CssBaseline";
import Typography from "@material-ui/core/Typography/Typography";
import Chip from "@material-ui/core/Chip/Chip";
import {Fave, User} from "../../types/user";
import produce from "immer";
import {Address} from "../../types/address";
import {CookingDate, Recipe} from "../../types/cooking-date";

import { v1 as uuidv1 } from 'uuid';
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import {getRecipeById} from "../recipe-api/recipe-api";
import FormControl from "@material-ui/core/FormControl/FormControl";





export const CreateCookingDate = () => {


    const useStyles = makeStyles((theme) => ({
        paper: {
            marginTop: theme.spacing(8),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        },
        form: {
            width: '100%',
            marginTop: theme.spacing(3),
        },
        submit: {
            margin: theme.spacing(3, 0, 2),
        },
        container: {
           backgroundSize: 'cover',
            margin: '0',
        },
        backgroundLight: {
            backgroundColor: '#fff',
            margin: 'auto',
            padding: '3.5%'


        },
        chips: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        chip: {
            margin: 2,
        },

        select: {
            width: '100%',
        },
        selectRecipe: {
            width: '50vw',
            height: '40px'
        }


    }));



    const classes = useStyles();


    const[cookingDate, setCookingDate] = useState<CookingDate>();
    //const[Address, setAddress] = useState<Address>();


    const [participants, setParticipants] = useState<{ id: string }[]>([]);
    const [recipe, setRecipe] = useState<Recipe | unknown>(undefined);


    const { register, handleSubmit, control } = useForm(); //2
    //const [participants, setParticipants] = useState<string[]>([]);
    const [currentUserId, setCurrentUserId] = useState('');
    const [currentUserFromDB, setCurrentUserFromDB] = useState<User>();
    const [favourites, setFavourites] = useState<Fave[]>([]);



    const onSubmit = (data: any) => {


        let newAddress= {
            street: data.street,
            number: data.number,
            postcode: data.postcode,
            city: data.city
        };


        setCookingDate({
            id:  uuidv1(),
            date: data.date,
            time: data.time,
            address: newAddress,
            users: participants as [{ id: string; }] ,
            recipes: recipe as Recipe,
        });

        if(cookingDate){
            createCookingDateByUser(currentUserId, cookingDate.id, cookingDate);
        }

        console.log("submit");


    }
    //const onSubmit = (data: any) => console.log(data.id, data, participants);


    //let [users, setUsers] = useState<[string, User][]>([]);
    let [users, setUsers] = useState<User[]>([]);



    const handleRecipe = (event: React.ChangeEvent<{ value: unknown }>) => {
        console.log(getRecipeById(event.target.value as string));

        const recipeId = event.target.value as string;
        if (recipeId) {
            getRecipeById(recipeId.toString()).then(response => {
                if (response) {
                    setRecipe(response)
                }
            }).catch(error => console.error(error))
        }

    };

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setParticipants(event.target.value as { id: string }[]);
    };

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };



    useEffect(()=>{

        const user =  firebase.auth().currentUser;
        if(user) {
            setCurrentUserId(user.uid);

            getUserById(user.uid).then((user: User) => {
                for (const [key, value] of Object.entries(user.favourites)) {
                    setFavourites(
                        produce(draft => {
                        draft.push(value)
                    }))
                }
                console.log(user.favourites);
            }).catch(error => {
                console.error(error)
                return null
            })

        }

        function getStyles(name: string, personName: string[], theme: Theme) {
            return {
                fontWeight:
                    personName.indexOf(name) === -1
                        ? theme.typography.fontWeightRegular
                        : theme.typography.fontWeightMedium,
            };
        }


        getAllUsers().then(allUsers => {
            for (const [key, value] of Object.entries(allUsers)) {
                if (!users.find(element => element.id.toString() === key)) {
                    setUsers(
                        produce(draft => {
                            draft.push(value)
                        })
                    )
                }
            }
        })


    }, []);

    return (
        <Container className={classes.container} component="main">
            <Container className={classes.backgroundLight} maxWidth="xs">
                <CssBaseline/>
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5" data-testid='header'>
                        Create a new Cooking Date
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={handleSubmit(onSubmit)}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    data-testid='createDate'
                                    name="date"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="date"
                                    label="Cooking Date"
                                    autoFocus
                                    inputRef={register}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="time"
                                    label="Eat O'Clock"
                                    name="time"
                                    inputRef={register}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <h3>Freunde hinzufügen</h3>
                                {users && (
                                    <Select
                                        labelId="demo-mutiple-chip-label"
                                        id="demo-mutiple-chip"
                                        multiple
                                        className={classes.select}
                                        value={participants}
                                        onChange={handleChange}
                                        input={<Input id="select-multiple-chip"/>}
                                        renderValue={(selected) => (
                                            <div className={classes.chips}>
                                                {(selected as User[]).map((value, key) => (
                                                    <Chip key={key} label={value} className={classes.chip}/>
                                                ))}
                                            </div>
                                        )}
                                        MenuProps={MenuProps}
                                        inputRef={(ref:any) => {
                                            if (!ref) return;
                                            register({
                                                name: ref.toString(),
                                                key:ref.key,
                                                value:ref.key,
                                            });
                                        }}
                                    >
                                        {users?.map((user) => (
                                            <MenuItem key={user.id} value ={user.id}>
                                                {user.firstName}
                                            </MenuItem>
                                        ))}

                                    </Select>
                                )}

                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="street"
                                    label="Street"
                                    name="street"
                                    inputRef={register}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="number"
                                    label="Number"
                                    name="number"
                                    inputRef={register}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="postcode"
                                    label="Postcode"
                                    name="postcode"
                                    inputRef={register}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="city"
                                    label="City"
                                    name="city"
                                    inputRef={register}
                                />
                            </Grid>
                        </Grid>
                        <Grid>


                            <h3>Choose Recipe</h3>
                             <Select
                                labelId="select-label"
                                id="select"
                                className={classes.selectRecipe}
                                value={recipe}
                                onChange={handleRecipe}
                            >
                                <MenuItem value="" disabled>
                                    Select from favourites
                                </MenuItem>
                                {favourites?.map(({id, recipeName}:any, index:number) => (
                                    <MenuItem key={index} value={id}>
                                        {recipeName}
                                    </MenuItem>
                                ))}
                            </Select>
                        </Grid>


                        <Button
                            type="submit"
                            data-testid='submitCreation'
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onSubmit={handleSubmit(onSubmit)}
                        >
                            Create new Cooking Date
                        </Button>
                    </form>
                </div>
            </Container>
        </Container>

    );
}


