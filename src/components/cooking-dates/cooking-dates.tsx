import firebase from "firebase";
import {Button, List, ListItem, Typography} from "@material-ui/core";
import React, {useEffect, useState} from "react";
import {CookingDate} from '../../types/cooking-date';
import {getAllCookingDates, getCookingDateById, getUserById} from "../../db/db";
import {User} from "../../types/user";
import Grid from "@material-ui/core/Grid/Grid";
import produce from "immer";
import Paper from "@material-ui/core/Paper/Paper";
import Container from "@material-ui/core/Container/Container";
import {valueToNode} from "@babel/types";
import {Router} from "react-router";
import { Link } from 'react-router-dom';

export const CookingDates = () => {


    let [cookingDates, setCookingDates] = useState<{ id: string }[]>([]);
    let [cookingDatesByUser, setCookingDatesByUser] = useState<CookingDate[]>([]);


    let currentUser = firebase.auth().currentUser;
    let dateArray:CookingDate[] = [];




   useEffect(()=>{

        if(currentUser){
            console.log(getUserById(currentUser.uid));

        getUserById(currentUser.uid).then((user) => {
            console.log(user.cookingdates);

            for (const [key, value] of Object.entries(user.cookingdates)) {
                console.log(value.id + "value");
                    getCookingDateById(value.id)?.then(date => {
                        console.log(date);
                        setCookingDatesByUser(produce(draft => {
                            draft.push(date)
                        }))
                    }).catch(error => console.error(error))
            }
            })
            .catch(error => {
                    console.log(error.message);
            })
        }
    }, [])

    const getDates = (dates:CookingDate[]) => {
        let content = [];
        for (const [key, value] of Object.entries(dates)) {
            content.push(

                    <Paper>
                    <h3>Cooking Date at the {value.date} at {value.time} O'Clock</h3>
                    <h4>Location:</h4>
                    <p>Street: {value.address.street}  Number: {value.address.number}</p>
                    <p>Postcode: {value.address.postcode}  City: {value.address.city}</p>
                        <Button variant="contained" color="primary" component={Link} to={`/date-detail/${value.id}`}>
                            Details
                        </Button>
                    </Paper>
                );
        }
        return content;
    };

    return <Container>{getDates(cookingDatesByUser)}</Container>;
}
