import React from 'react';
import {BrowserRouter, Switch, Route, Link} from "react-router-dom";
import {CookingDatesDetail} from './components/cooking-dates-detail/cooking-dates-detail';
import {HomeScreen} from './components/home/home';
import {theme} from './shared/styles/theme';
import {ThemeProvider} from 'styled-components';
import {
    AppBar,
    BottomNavigation,
    BottomNavigationAction, createStyles, makeStyles,
    MuiThemeProvider, Theme,
    Typography
} from '@material-ui/core';
import {Login} from './components/authentication/login';
import {Register} from './components/authentication/register';
import {Provider} from 'react-redux';
import {store} from './store'
import {RecipeSearch} from "./components/recipe-search/recipeSearch";
import {Profile} from './components/profile/profile';
import {AddCircleOutline, FavoriteBorder, Home, Search, Event, AccountCircle} from '@material-ui/icons';
import firebase from "firebase";
import Button from "@material-ui/core/Button/Button";
import GuardedRoute from './GuardedRoute';
import {CookingDates} from './components/cooking-dates/cooking-dates';
import {StyledToolbar} from './shared/styles/toolbar';
import {Favourites} from './components/favourites/favourites'


function App() {
    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            root: {
                flexGrow: 1,
            },
            menuButton: {
                marginRight: theme.spacing(2),
            },
            title: {
                flexGrow: 1,
            },
        }),
    );

    const [value, setValue] = React.useState(0);
    const classes = useStyles();
    //let currentUser = firebase.auth().currentUser;

    return (
        <Provider store={store}>
            <MuiThemeProvider theme={theme}>
                <ThemeProvider theme={theme}>
                    <AppBar position="static">
                        <StyledToolbar>
                            <Typography variant="h6" className={classes.title}>
                                Eat with me
                            </Typography>
                            {firebase.auth().currentUser && (
                                <div>
                                    <AccountCircle />
                                </div>
                            )}
                            {!firebase.auth().currentUser && (
                                <div>
                                    <Button color="inherit">Login</Button>
                                </div>
                            )}

                        </StyledToolbar>
                    </AppBar>

                    <BrowserRouter>
                        <Link to='/'>Home</Link> |&nbsp;
                        <Link to='/profile'>Profile</Link> |&nbsp;
                        <Link to='/login'>Login</Link> |&nbsp;
                        <Link to='/register'>Register</Link> |&nbsp;
                        <Link to='/cooking-dates'>Cooking Dates</Link> |&nbsp;
                        <Link to='/recipe-search'>Recipe Search</Link> |&nbsp;
                        <Link to='/favourites'>Favourites</Link>

                        <Switch>
                            <Route path='/' component={HomeScreen} exact/>
                            <GuardedRoute exact path="/profile" component={Profile} auth={firebase.auth().currentUser}/>
                            <Route exact path="/login" component={Login} />
                            <Route path="/register" component={Register} />

                            <GuardedRoute path={`/cooking-dates`} component={CookingDates} auth={firebase.auth().currentUser}/>
                            <Route path={`/date-detail/:id`} component={CookingDatesDetail}/>
                            <GuardedRoute path={`/recipe-search`} component={RecipeSearch} auth={firebase.auth().currentUser} />
                            <GuardedRoute path={`/favourites`} component={Favourites} auth={firebase.auth().currentUser} />
                        </Switch>
                    </BrowserRouter>


                    <BottomNavigation
                        value={value}
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                        showLabels>
                        <BottomNavigationAction label="Home" icon={<Home/>} data-testid="homeTab"/>
                        <BottomNavigationAction label="Recipes" icon={<Search/>} data-testid="recipesTab"/>
                        <BottomNavigationAction label="Favorites" icon={<FavoriteBorder/>} data-testid="favoritesTab"/>
                        <BottomNavigationAction label="MyDates" icon={<Event />} />
                        <BottomNavigationAction label="Add Date" icon={<AddCircleOutline/>} />
                        <BottomNavigationAction label="Profile" icon={<AccountCircle />} />
                    </BottomNavigation>
                </ThemeProvider>
            </MuiThemeProvider>
        </Provider>

    );
}

export default App;
