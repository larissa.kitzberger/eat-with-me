import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import firebase from "firebase/app";
import "firebase/auth";
import './global.scss';

/**
 * firebase initialization
 */
const app = firebase.initializeApp({
    apiKey: "AIzaSyCBGl3zvEF76GnARP8UkpCOfX7B1kak8Ik",
    authDomain: "eat-with-me-1d32e.firebaseapp.com",
    databaseURL: "https://eat-with-me-1d32e-default-rtdb.europe-west1.firebasedatabase.app/",
    projectId: "eat-with-me-1d32e",
    storageBucket: "eat-with-me-1d32e.appspot.com",
    messagingSenderId: "333083075508",
    appId: "1:333083075508:web:4df700df063ccd096c48e1"
});



export const auth = app.auth();



const rootElement = document.getElementById("root");
ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    rootElement
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


