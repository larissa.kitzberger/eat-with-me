import {colors, createMuiTheme} from '@material-ui/core';
import { green, orange } from '@material-ui/core/colors';

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#b5c99a',
        },
        secondary: {
            main: '#718355'
        },
        success: {
            main: '#000000',
        }
    }
})

