import styled from "styled-components";
import {Toolbar} from '@material-ui/core';

export const StyledToolbar = styled(Toolbar)`
  ${({theme}) => `
    background-color: ${theme.palette.primary.main};
    color: white;
    .root {
      flexGrow: 1;
    }
    .title {
      flexGrow: 1; 
    }
    .backButton: {
      marginRight: theme.spacing(2),
    }
  `}
`;
