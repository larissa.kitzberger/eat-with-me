import styled from 'styled-components';
import {Button} from '@material-ui/core';

export const StyledButton = styled(Button)`
  ${({theme}) => `
    color: ${theme.palette.primary.main};
    alignSelf:'center',
  `}
`;
