import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const LinkButton = styled(Link)`
    color: white;
`;
