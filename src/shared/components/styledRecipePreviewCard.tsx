import styled from 'styled-components';
import {Card,CardMedia} from '@material-ui/core';


export const StyledRecipePreviewCard = styled(Card)`
    max-width: 100%;
    display: block;
    margin-top: 5px;
    margin-bottom:5px;
    text-align: center;
`;

