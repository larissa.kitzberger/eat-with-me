import {AppBar, IconButton, Typography} from "@material-ui/core";
import {StyledToolbar} from "../styles/toolbar";
import React from "react";


export interface EatWithMeAppBarProbs {
    siteName: string
}


export const EatWithMeAppBar = ({ siteName }: EatWithMeAppBarProbs) => {

    return(
        <AppBar position='static'>
            <StyledToolbar>
                <IconButton edge='start' className='backButton' color='inherit'>
                </IconButton>
                <Typography variant='h6' className='title'>
                    {siteName}
                </Typography>
            </StyledToolbar>
        </AppBar>
    )
}
