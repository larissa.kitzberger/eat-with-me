import { render } from '@testing-library/react'
import {User} from "../../types/user";

import {Typography} from "@material-ui/core";
import React from "react";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import Favorite from "@material-ui/icons/Favorite";
import {withStyles} from "@material-ui/core/styles";
import Checkbox, {CheckboxProps} from "@material-ui/core/Checkbox";


describe("recipePreview", () => {
    const id: string = "756817"
    const title: string = "Matcha Pancakes"
    const image: string = "https://spoonacular.com/…mages/756817-556x370.jpg"
    const servings: string = "2"
    const readyInMinutes: string = "45"
    const currentUser: User = {
        id: 5,
        firstName: "Lucy",
        lastName: "Schach",
        email: "lucy@test.de",
        password: "passwort",
        address: {
            city: "City",
            number: 15,
            postcode: 4232,
            street: "Street"
        },
        favourites: [{id: "654959"}],
        cookingdates: [{id: ""}]
    }

    test("create title", () => {

        const {getByRole, queryByText } = render(
        <Typography variant="h6" component="h1">{title}</Typography>
        )

        expect(getByRole('heading')).toBeDefined()
        expect(queryByText("Matcha Pancakes")).toBeDefined()
    })

    test("Checkbox", () => {
        const FavoriteCheckbox = withStyles({
            root: {
                color: '#b7094c',
                '&$checked': {
                    color: '#b7094c',
                },
            },
            checked: {},
        })((props: CheckboxProps) => <Checkbox color="default" {...props} />);
        const decideFavorite = (id: string): boolean => {
            for (const [key, value] of Object.entries(currentUser.favourites)) {
                if (id === value.id.toString()) {
                    return true
                }
            }
            return false
        }

        const {getByRole, queryByText} = render(
            <FavoriteCheckbox checked={decideFavorite("756817")}
                                           icon={<FavoriteBorder/>} checkedIcon={<Favorite/>}
                                           name="Fave"/>
        )

        const checkbox = getByRole('checkbox')
        expect(getByRole('checkbox')).toBeDefined()

    })
})