import React, {useEffect} from "react";
import {useState} from 'react';
import {ExtendedIngredient, RecipeLong, RecipeShort} from '../../types/recipeResponse'
import {Fave, User} from '../../types/user'
import {Typography, makeStyles, Chip, CardActions, CardContent, Grid} from '@material-ui/core';
import ScheduleIcon from "@material-ui/icons/Schedule";
import GroupIcon from "@material-ui/icons/Group";
import {createStyles, Theme, withStyles} from '@material-ui/core/styles';
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import firebase from "firebase";
import {StyledRecipePreviewCard} from "../../shared/components/styledRecipePreviewCard";
import CardMedia from "@material-ui/core/CardMedia";
import {StyledButton} from "../styles/button";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import Favorite from "@material-ui/icons/Favorite";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {getUserById, updateFavorites} from "../../db/db";
import {FullRecipe} from "./fullRecipe";



const defaultProps = {
    bgcolor: 'background.paper',
    borderColor: 'text.primary',
    border: 1,
    padding: 5,
};

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

const FavoriteCheckbox = withStyles({
    root: {
        color: '#b7094c',
        '&$checked': {
            color: '#b7094c',
        },
    },
    checked: {},
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);


export interface RecipePreviewProps {
    id: string
    title: string
    image: string
    servings: string
    readyInMinutes: string
    currentUser: User
    recipeDescription: string
    ingredients: ExtendedIngredient[] | null
    instructions: string[]
}


export const RecipePreview = ({ id, title, image, servings, readyInMinutes, currentUser, recipeDescription,ingredients, instructions }: RecipePreviewProps) => {
    let currentFirebaseUser = firebase.auth().currentUser;
    const [currentUserSnapshot, setCurrentUserSnapshot] = useState<User>(currentUser);
    const [openedRecipeInfo, setOpenedRecipeInfo] = useState<boolean>(false)

    const [faveObject, setFaveObject] = useState<Fave>({id:id,recipeName:title});


    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            root: {
                width: '30vw',
                margin: '1vw'
            },
            media: {
                height: 0,
                paddingTop: '56.25%', // 16:9
            },

            recipeCardsContainer:{
                display: 'flex',
                width: '100%',
                flexWrap: 'wrap',
                margin:'auto',
            },
        }),
    );

    const classes = useStyles();

    useEffect( () => {


    },[])


    const decideFavorite = (id: string):boolean => {
        for (const [key, value] of Object.entries(currentUserSnapshot.favourites)) {
            if(id === value.id.toString()){
                return true
            }
        }
        return false
    }

    const handleFaveButtonEvent = (id: string, event:any) => {
        if(currentUserSnapshot) {
            if (decideFavorite(id)) {
                updateFavorites(currentUserSnapshot.id.toString(), id, null).then(() => {

                }).catch(error => console.error(error))
            } else {
                updateFavorites(currentUserSnapshot.id.toString(), id, faveObject).then(() => {

                }).catch(error => console.error(error))
            }
            getUserById(currentUserSnapshot.id.toString()).then(response => {
                setCurrentUserSnapshot(response);
                console.log('saveCurrentUser')
            }).catch(error => console.error(error))
        }
    }

    const closeRecipeDetailView = () => {
        setOpenedRecipeInfo(false)
    }

    const setFaveLabel = (isFavorite: boolean): string => {
        if(isFavorite){
            return "already a favourite"
        } else {
            return "add to favourites"
        }
    }
    return (
        <StyledRecipePreviewCard>
            <CardActions>
                <FormControlLabel
                    control={<FavoriteCheckbox checked={decideFavorite(id.toString())}
                                               icon={<FavoriteBorder/>} checkedIcon={<Favorite/>}
                                               onChange={(e) => handleFaveButtonEvent(id.toString(), e)}
                                               name="Fave"/>}
                    label={setFaveLabel(decideFavorite(id.toString()))}/>
            </CardActions>
            <CardMedia
                className={classes.media}
                image={image}
                title={title}
            />
            <CardContent>
                <Chip icon={<ScheduleIcon/>} label={readyInMinutes + ' min'} color="secondary"/>&nbsp;
                <Chip icon={<GroupIcon/>} label={servings} color="secondary"/>
                <br/>&nbsp;
                <Typography variant="h6" component="h1">{title}</Typography>
                <FullRecipe open={openedRecipeInfo}
                            recipeDescription={recipeDescription}
                            onClose={closeRecipeDetailView}
                            id={id}
                            title={title}
                            image={image}
                            readyInMinutes={readyInMinutes}
                            servings={servings}
                            ingredients={ingredients}
                            instructions={instructions}
                />
            </CardContent>
            <CardActions>

                <StyledButton size="large" onClick={() => setOpenedRecipeInfo(true)}>See Details</StyledButton>


            </CardActions>
        </StyledRecipePreviewCard>
    )
}