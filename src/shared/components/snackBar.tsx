import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';


export interface SuccessSnackBarProps {
    open: boolean
    setOpen: (isOpen: boolean) => void
    message: string
    type: 'success' | 'error'
}

export const SnackBar = (props: SuccessSnackBarProps) => {
    const {open, setOpen} = props

    const handleClose = () => {
        setOpen(false)
    }

    return (
        <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
            <Alert severity={props.type}>{props.message}</Alert>
        </Snackbar>
    )

}
