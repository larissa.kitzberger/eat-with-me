import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import {CardContent, Chip, DialogActions, DialogContent, Grid, ListItem, TextField, Typography} from '@material-ui/core';
import React from 'react';
import {ExtendedIngredient, ExtendedSteps, RecipeLong, Result} from "../../types/recipeResponse";
import ScheduleIcon from "@material-ui/icons/Schedule";
import GroupIcon from "@material-ui/icons/Group";
import {StyledPreviewImage} from "./styledPreviewImage";

export interface FullRecipeProps {
    open: boolean;
    recipeDescription: string
    onClose: () => void
    id: string
    title: string
    image: string
    readyInMinutes: string
    servings: string
    ingredients: ExtendedIngredient[] | null
    instructions: string[]
}

export const FullRecipe = (props: FullRecipeProps) => {
    const { open, recipeDescription, onClose, id, title, image, readyInMinutes, servings,ingredients, instructions } = props


    const handleClose = () => {
        onClose()
    }


    return(
        <Dialog open={open} onClose={() => handleClose()} aria-labelledby={id}
                maxWidth="sm" fullWidth={true} >
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={4} xl={3}>
                        <StyledPreviewImage src={image} alt="recipe-image" />
                    </Grid>
                    <Grid item xs={12} sm={4} xl={3}>
                        <br/>
                        <Chip icon={<ScheduleIcon/>} label={readyInMinutes + ' min'} color="secondary"/>&nbsp;
                        <Chip icon={<GroupIcon/>} label={servings} color="secondary"/>
                        { ingredients?.map(ingredient =>
                            <Chip label={ingredient.originalString} color="primary"/> )}
                    </Grid>
                    <div
                        dangerouslySetInnerHTML={{
                            __html: recipeDescription
                        }}>
                    </div>
                    <div>
                        {instructions?.map(instruction =>
                            <ListItem>
                                <li>
                                    <Typography variant="body2">{instruction}</Typography>
                                </li>
                            </ListItem>
                        )}
                    </div>
                </Grid>
            </DialogContent>
        </Dialog>
    );
}
