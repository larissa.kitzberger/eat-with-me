import styled from 'styled-components';


export const StyledPreviewImage = styled.img`
  box-shadow: 0px 10px 24px -4px rgba(0,0,0,0.38);
  margin-bottom: 10px;
  width: 100%;
  height: auto;
`;
