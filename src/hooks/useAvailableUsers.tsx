import {useSelector} from "react-redux";
import {RootState} from '../store';
import {useMemo, useState} from "react";
import {getAllUsers} from '../db/db';
import {User} from '../types/user';


export const useAvailableUsers = () => {
    const { users } = useSelector((state: RootState) => state.user)
    const [allUsers, setAllUsers] = useState<User[]>([])

    return useMemo(() => {
        console.log(users)
        getAllUsers().then(allUsers => {
            setAllUsers(allUsers)
        })
       return allUsers.filter(user => !users.find((element: User) => element.id === user.id))
    }, [users, allUsers])
}

