# Eat-with-me

Team Members:
Lucy Schach (S2010629008)
Julia Hainz (S2010629004)
Larissa Kitzberger (S2010629005)


Idea:
Applikation für gemeinsames Kochen mit Freunden.
Resteverwertung für Freunde
Beispiel API: https://spoonacular.com/food-api/docs 

Basic features:
Suchfunktion für Rezepte nach Suchbegriff
Filterfunktion der Rezepte (vegetarisch, vegan, Unverträglichkeiten)
Favoritenliste für gelungene Rezepte
Zutatenliste für das gewählte Rezept, wo angegeben werden kann, wer welche Zutat mitbringt
Freunde suchen, verwalten und zum Termin hinzufügen 
Login und Authentifizierung

# Nutzung

Nutzung:
nach npm install und npm start kann der User registriert werden und eingelogged.
Der automatische redirect landet auf Home.

Home
Hier werden die nächsten Cooking Dates ausgegeben und neue Dates können angelegt werden

Navigation
Um auf die Subseiten zu gelangen, muss nach dem Login oder beim Reload zuerst unten im Menü, auf ein Icon geklickt werden (momentan keine Funktionalität). Danach können die Links oben auf dem Bildschirm verwendet werden, um auf die Subseiten zu navigieren.

Recipe Search:
Hier kann durch Eingabe eines englischen Suchbegriffs Rezepte an der API angefragt werden.
Wenn der Haken bei vegan/vegetarisch/lactosefrei gesetzt wird, wird eine ensprechende suche gestartet. Über Das Herz bei jeder Rezeptanzeige, kann ein Favourit in der Firebase Datenbank gesetzt oder entfernt werden.
Über den Details Button, wird eine ausführlichere Anzeige des Rezeptes geladen.

Favourites:
Alle Favouriten sind auf der Favouriten Subseite angezeigt

Date Detail:
Hier wird ein Beispiel für ein einzelnes Cooking-Date angezeigt.
Teilnehmer und Ort des Cooking Dates kann angepasst werden.
In der Zutaten-Liste kann durch entsprechendes klicken der Einkauftasche in der Datenbank & dem Interface vermerkt und angezeigt werden, wer welche Zutaten einkauft

Profil:
Hier werden die Adresse und Name des Users angezeigt


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
